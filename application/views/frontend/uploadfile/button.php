<div class="btn-group">
	<a href="<?= base_url($global->url.'download/'.$row->file_file)?>" class="detail btn btn-flat btn-xs btn-warning <?= $global->download!=true ? 'disabled':'' ?>"><span class="fa fa-download"></span></a>
	<a href="#" id="<?=$row->file_id?>" url="<?= base_url($global->url.'edit')?>" class="edit btn btn-flat btn-xs btn-info <?= $global->edit!=true ? 'disabled':'' ?>"><span class="fa fa-pencil"></span></a>
	<a href="#"   url="<?=base_url($global->url.'hapus/'.$row->file_id)?>"  class="hapus btn btn-flat btn-xs btn-danger <?= $global->delete!=true ? 'disabled':'' ?>"><span class="fa fa-trash"></span></a>
</div>