<div id="view">
	<div class="row">
		<div class="col-sm-2">
			<div class="form-group">
				<button data-toggle="modal"  data-target="#modaladdfile" class="btn btn-flat btn-block btn-primary"><span class="fa fa-plus"></span> Tambah</button>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div id="tabel" url="<?= base_url($global->url.'tabel')?>">
				<div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading data. Please wait...</div>
			</div>
		</div>		
	</div>
</div>
<div class="modal fade" id="modaladdfile">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Form Upload File</h4>
      </div>
      <form method="POST" action="<?= base_url($global->url)?>" enctype="multipart/form-data">
      <div class="modal-body">
      	   <div class="form-group">
      	   		<label>Tanggal Upload</label>
      	   		<input required readonly type="text" class="form-control" name="file_date" value="<?= date('d-m-Y')?>"></input>
      	   </div>      	   
      	   <div class="form-group">
      	   		<label>Nama File</label>
      	   		<input required type="text" class="form-control" name="file_nama"></input>
      	   </div>
      	   <div class="form-group">
      	   		<label>File</label>
      	   		<input required type="file" name="file_file"></input>
      	   		<p class="help-block">Berkas format PDF, ukuran max 5mb</p>
      	   </div>
      </div>
      <div class="modal-footer">
        <button type="submit" value="submit" name="submit" class="btn bg-blue btn-flat btn-block">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!--MODAL EDIT-->
<div id="modaledit"></div>
<?php include 'action.php';?>
<script type="text/javascript">
	    setTimeout(function () {
        var url=$('#tabel').attr('url');
        $("#tabel").load(url);
        //alert(url);
    }, 200); 
</script>