<div class="modaledit modal fade" id="modaladdfile">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-orange">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?= ucwords($global->headline)?></h4>
      </div>
      <form method="POST" action="<?= base_url($global->url.'edit')?>" enctype="multipart/form-data">
      <div class="modal-body">
            <div class="form-group">
              <label>Id</label>
              <input type="text" readonly="readonly" class="form-control" name="id" value="<?= $data->file_id?>"></input>
            </div>
      	   <div class="form-group">
      	   		<label>Tanggal Upload</label>
      	   		<input required readonly type="text" class="form-control" name="file_date" value="<?= date('d-m-Y')?>"></input>
      	   </div>      	   
      	   <div class="form-group">
      	   		<label>Nama File</label>
      	   		<input required type="text" class="form-control" name="file_nama" value="<?=$data->file_nama?>"></input>
      	   </div>
      	   <div class="form-group">
      	   		<label>File</label>
      	   		<input required type="file" name="file_file"></input>
      	   		<p class="help-block">Berkas format PDF, ukuran max 5mb</p>
              <a href="<?= base_url($global->url.'download/'.$data->file_file)?>" class="btn btn-xs btn-success btn-flat"><i class="fa fa-cloud-download"></i> File Tersimpan</a>
      	   </div>
      </div>
      <div class="modal-footer">
        <button type="submit" value="submit" name="submit" class="btn bg-orange btn-flat btn-block">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>