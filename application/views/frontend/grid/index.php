<style type="text/css">
* { box-sizing: border-box; }

body { font-family: sans-serif; }

/* ---- grid ---- */

.grid {
  background: #EEE;
  max-width: 1200px;
}

/* clearfix */
.grid:after {
  content: '';
  display: block;
  clear: both;
}

/* ---- grid-item ---- */

.grid-item {
  width: 160px;
  height: 120px;
  float: left;
  background: #D26;
  border: 2px solid #333;
  border-color: hsla(0, 0%, 0%, 0.5);
  border-radius: 5px;
}

.grid-item--width2 { width:  320px; }
.grid-item--width3 { width:  480px; }
.grid-item--width4 { width:  720px; }

.grid-item--height2 { height: 200px; }
.grid-item--height3 { height: 260px; }
.grid-item--height4 { height: 360px; }

</style>
<div id="view">
	<div class="row">
		<div class="col-sm-2">
			<div class="form-group">
				<button data-toggle="modal"  data-target="#modaladdfile" class="btn btn-flat btn-block btn-primary"><span class="fa fa-plus"></span> Tambah</button>
			</div>
		</div>
	</div>
  <!--
	<div class="row">
		<div class="col-sm-12">
			<div id="tabel" url="<?= base_url($global->url.'tabel')?>">
				<div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading data. Please wait...</div>
			</div>
		</div>		
	</div>
  -->
  <div class="row">
    <div class="col-sm-12">
      <h1>Masonry - itemSelector</h1>
      <div class="grid">
        <div class="static-banner">Static banner</div>
        <div class="grid-item"></div>
        <div class="grid-item grid-item--width2 grid-item--height2"></div>
        <div class="grid-item grid-item--height3"></div>
        <div class="grid-item grid-item--height2"></div>
        <div class="grid-item grid-item--width3"></div>
        <div class="grid-item"></div>
        <div class="grid-item"></div>
        <div class="grid-item grid-item--height2"></div>
        <div class="grid-item grid-item--width2 grid-item--height3"></div>
        <div class="grid-item"></div>
        <div class="grid-item grid-item--height2"></div>
        <div class="grid-item"></div>
        <div class="grid-item grid-item--width2 grid-item--height2"></div>
        <div class="grid-item grid-item--width2"></div>
        <div class="grid-item"></div>
        <div class="grid-item grid-item--height2"></div>
        <div class="grid-item"></div>
        <div class="grid-item"></div>
        <div class="grid-item grid-item--height3"></div>
        <div class="grid-item grid-item--height2"></div>
        <div class="grid-item"></div>
        <div class="grid-item"></div>
        <div class="grid-item grid-item--height2"></div>
      </div> 
    </div>
  </div>
</div>
<div class="modal fade" id="modaladdfile">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Form Upload File</h4>
      </div>
      <form method="POST" action="<?= base_url($global->url)?>" enctype="multipart/form-data">
      <div class="modal-body">
      	   <div class="form-group">
      	   		<label>Tanggal Upload</label>
      	   		<input required readonly type="text" class="form-control" name="file_date" value="<?= date('d-m-Y')?>"></input>
      	   </div>      	   
      	   <div class="form-group">
      	   		<label>Nama File</label>
      	   		<input required type="text" class="form-control" name="file_nama"></input>
      	   </div>
      	   <div class="form-group">
      	   		<label>File</label>
      	   		<input required type="file" name="file_file"></input>
      	   		<p class="help-block">Berkas format PDF, ukuran max 5mb</p>
      	   </div>
      </div>
      <div class="modal-footer">
        <button type="submit" value="submit" name="submit" class="btn bg-blue btn-flat btn-block">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!--MODAL EDIT-->
<div id="modaledit"></div>
<?php include 'action.php';?>
<script type="text/javascript">
    $('.grid').masonry({
      itemSelector: '.grid-item',
      columnWidth: 160
    });

	    setTimeout(function () {
        var url=$('#tabel').attr('url');
        $("#tabel").load(url);
        //alert(url);
    }, 200); 
</script>