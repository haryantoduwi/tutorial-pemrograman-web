<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include controller master 
include APPPATH.'controllers/Master.php';

class Grid extends Master {
	public function __construct(){
		parent::__construct();
		$this->load->model('Crud');
		// if(($this->session->userdata('login')!=true) || ($this->session->userdata('level')!=1) ){
		// 	redirect(site_url('login/logout'));
		// }
	}
	//VARIABEL
	private $master_tabel="file"; //Mendefinisikan Nama Tabel
	private $id="file_id";	//Menedefinisaikan Nama Id Tabel
	private $default_url="frontend/grid/"; //Mendefinisikan url controller
	private $default_view="frontend/grid/"; //Mendefinisiakn defaul view
	private $view="template/webfrontend"; //Mendefinisikan Tamplate Root
	private $path='./upload/'; //Nama folder upload file

	private function global_set($data){
		$data=array(
			'menu'=>'grid',//Seting menu yang aktif
			'menu_submenu'=>false,
			'headline'=>$data['headline'], //Deskripsi Menu
			'url'=>$data['url'], //Deskripsi URL yang dilewatkan dari function
			'ikon'=>"fa fa-tasks",
			'view'=>"views/frontend/grid/index.php",
			'detail'=>false,
			'cetak'=>false,
			'edit'=>true,
			'delete'=>true,
			'download'=>true,
		);
		return (object)$data; //MEMBUAT ARRAY DALAM BENTUK OBYEK
		//$data->menu=grid, bentuk obyek
		//$data['menu']=grid, array bentuk biasa
	}

	public function index()
	{
		$global_set=array(
			'headline'=>'grid',
			'url'=>$this->default_url,
		);
		$global=$this->global_set($global_set);
		if($this->input->post('submit')){
			//PROSES SIMPAN
			$data=array(
				'file_nama'=>$this->input->post('file_nama'),
				'file_date'=>date('Y-m-d',strtotime($this->input->post('file_date'))),
			);
			$file='file_file';
			if($_FILES[$file]['name']){
				///PROSES UPLOAD FILE
				if($this->fileupload($this->path,$file)){
					//TIDAK ADA ERROR
					$namafile=$this->upload->data('file_name');
					$data['file_file']=$namafile;
				}else{
					//ERROR
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url($this->default_url));
				}
			}
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
			);
			$insert=$this->Crud->insert($query);
			$this->notifiaksi($insert);
			redirect(site_url($this->default_url));
			//$this->dumpdata($data);
		}else{
			$data=array(
				'global'=>$global,
				'menu'=>$this->menu(0),
			);		
			$this->load->view($this->view,$data);
		}
	}
	public function tabel(){
		$global_set=array(
			'headline'=>false,
			'url'=>$this->default_url,
		);
		//LOAD FUNCTION GLOBAL SET
		$global=$this->global_set($global_set);		
		//PROSES TAMPIL DATA
		$query=array(
			'tabel'=>$this->master_tabel,
			'order'=>array('kolom'=>'file_date','orderby'=>'DESC'),
		);
		$data=array(
			'global'=>$global,
			'data'=>$this->Crud->read($query)->result(),
		);
		$this->load->view($this->default_view.'tabel',$data);		
		//$this->dumpdata($data['data']);
	}
	public function download($file){
		$path=$this->path;
		$this->downloadfile($path,$file);
	}
	private function get_file($id){
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array(array('file_id'=>$id)),
		);
		$read=$this->Crud->read($query)->row();
		if($read->file_file){//KOLOM FILE
			unlink($this->path.$read->file_file); //HAPUS FILE
		}
	}
	public function hapus($id){
		$query=array(
			'tabel'=>$this->master_tabel,
			'where'=>array('file_id'=>$id),
		);
		//FUNGSI HAPUS FILE
		$this->get_file($id);
		$delete=$this->Crud->delete($query);
		$this->notifiaksi($delete);
		redirect(site_url($this->default_url));
	}	
	public function edit(){
		$global_set=array(
			'headline'=>'edit data upload file',
			'url'=>$this->default_url,
		);
		$global=$this->global_set($global_set);
		//DATA YANG DILEWATKAN AJAX
		$id=$this->input->post('id');
		if($this->input->post('submit')){
			//PROSES UPDATE
			$data=array(
				'file_nama'=>$this->input->post('file_nama'),
				'file_date'=>date('Y-m-d',strtotime($this->input->post('file_date'))),
			);
			//PROSES UPLOAD FILE
			$file='file_file';
			if($_FILES[$file]['name']){
				///PROSES UPLOAD FILE
				if($this->fileupload($this->path,$file)){
					//FUNGSI HAPUS FILE
					$this->get_file($id);					
					//TIDAK ADA ERROR
					$namafile=$this->upload->data('file_name');
					$data['file_file']=$namafile;
				}else{
					//ERROR
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url($this->default_url));
				}
			}			
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
				'where'=>array($this->id=>$id),
			);
			$update=$this->Crud->update($query);
			$this->notifiaksi($update);
			redirect(site_url($this->default_url));
		}else{
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array($this->id=>$id))
			);
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
				'global'=>$global,
				'menu'=>$this->menu(0),
			);
			//$this->dumpdata($data['data']);			
			$this->load->view($this->default_view.'modaledit',$data);
		}			
	}	
	// public function add(){
	// 	$global_set=array(
	// 		'submenu'=>false,
	// 		'headline'=>'add data',
	// 		'url'=>$this->default_url, //AKAN DIREDIRECT KE INDEX
	// 	);	
	// 	$global=$this->global_set($global_set);
	// 	$data=array(
	// 		'global'=>$global,
	// 		);
	// 	$this->load->view($this->default_view.'add',$data);		
	// }	

}
