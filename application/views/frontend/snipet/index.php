<div id="view">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?= ucwords($global->headline)?></h3>
		</div>
		<div class="box-body">
			<p>Belajar Pemrograman Web</p>
			<ul>
				<li><a href="<?= site_url('frontend/uploadfile')?>">Upload File</a></li>
				<li><a href="#">Date Picker</a></li>
				<li><a href="<?= site_url('frontend/grid')?>">Grid</a></li>
				<li><a href="#">Select 2</a></li>
				<li><a href="#">Nomor Registrasi</a></li>
			</ul>
		</div>
	</div>
</div>
<?php include 'action.php';?>